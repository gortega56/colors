//
//  TileView.swift
//  Colors
//
//  Created by Gabriel Ortega on 11/3/14.
//  Copyright (c) 2014 Clique City. All rights reserved.
//

import UIKit

let equalityDelta:CGFloat = 0.03

class TileView: UIView, Equatable
{
    let angle:CGFloat = 30.0 * CGFloat(M_PI / 180.0)
    var indexPath:NSIndexPath
    var color:UIColor
    {
        didSet
        {
            backgroundColor = color
        }
    }
    
    convenience init(frame: CGRect, indexPath:NSIndexPath)
    {
        self.init(frame:frame)
        self.indexPath = indexPath
    }
    
    convenience init(frame: CGRect, color:UIColor, indexPath:NSIndexPath)
    {
        self.init(frame:frame)
        self.color = color;
        self.backgroundColor = color
        self.indexPath = indexPath
    }
    
    convenience init(frame: CGRect, color:UIColor)
    {
        self.init(frame:frame, color: color, indexPath: NSIndexPath(index: 0))
    }
    
    override init(frame: CGRect)
    {
        self.color = UIColor.whiteColor()
        self.indexPath = NSIndexPath(index: 0)
        super.init(frame: frame)
        
        self.layer.frame = CGRectInset(frame, 0, 0)
//        self.layer.cornerRadius = 5
//        self.layer.masksToBounds = true

    }
    
    required init(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    func rotateAboutAxis(axis:CGVector)
    {
        alpha = 0.8
        layer.zPosition = 0
        layer.transform = CATransform3DScale(rotationTransformAboutAxis(axis), 1.5, 1.5, 1)
    }
    
    func rotationTransformAboutAxis(axis:CGVector) -> (CATransform3D)
    {
        var transform:CATransform3D = CATransform3DIdentity
        transform.m11 = ((1 - cos(angle)) * (axis.dx * axis.dx)) + cos(angle)           // 1-cos(theta)x^2 + cos(theta)
        transform.m12 = ((1 - cos(angle)) * (axis.dx * axis.dy))                        // 1-cos(theta)xy + sin(theta)z = 1-cos(theta)xy
        transform.m13 = -(sin(angle) * axis.dy)                                            // 1-cos(theta)xz - sin(theta)y = -sin(theta)y
        transform.m14 = 0
        transform.m21 = ((1 - cos(angle)) * (axis.dx * axis.dy))                        // 1-cos(theta)xy - sin(theta)z = 1-cos(theta)xy
        transform.m22 = ((1 - cos(angle)) * (axis.dy * axis.dy)) + cos(angle)           // 1-cos(theta)y^2 + cos(theta)
        transform.m23 = (sin(angle) * axis.dx)                                           // 1-cos(theta)yz + sin(theta)x = sin(theta)x
        transform.m24 = 0
        transform.m31 = (sin(angle) * axis.dy)                                           // 1-cos(theta)xz + sin(theta)y = sin(theta)y
        transform.m32 = -(sin(angle) * axis.dx)                                            // 1-cos(theta)yz - sin(theta)x = -sin(theta)x
        transform.m33 = cos(angle)                                                          // 1-cos(theta)z^2 + cos(theta) = cos(theta)
        transform.m34 = (1.0 / -1000)
        transform.m41 = 0
        transform.m42 = 0
        transform.m43 = 0
        transform.m44 = 1
        return transform;
    }
}

func == (left:TileView, right:TileView) -> Bool
{
    var leftHue:CGFloat = 0;
    var rightHue:CGFloat = 0;
    
    left.backgroundColor?.getHue(&leftHue, saturation: nil, brightness: nil, alpha: nil);
    right.backgroundColor?.getHue(&rightHue, saturation: nil, brightness: nil, alpha: nil);

    return ((fabs(leftHue - rightHue)) <= equalityDelta)
}
