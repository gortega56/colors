//
//  PuzzleViewController.swift
//  Colors
//
//  Created by Gabriel Ortega on 1/6/15.
//  Copyright (c) 2015 Clique City. All rights reserved.
//

import UIKit

class PuzzleViewController: UIViewController
{
    let selectionView:TileView = TileView(frame: CGRectZero)
    var gridView:PuzzleGridView?
    var gestureStartPoint:CGPoint = CGPointMake(0, 0)
    var sourceView:TileView = TileView(frame: CGRectZero)
    
    override func loadView()
    {
        var view:UIView = UIView(frame: UIScreen.mainScreen().bounds)
        
        var topView:UIView = UIView(frame: CGRectMake(0, 0, CGRectGetWidth(view.frame), CGRectGetMidY(view.frame) - CGRectGetWidth(view.bounds) / 3 * 1))
        topView.backgroundColor = UIColor(hue: 0.95, saturation: 0.75, brightness: 0.7, alpha: 1)
        view.addSubview(topView)
        
        var topPuzzleView = PuzzleGridView(frame: CGRectMake(0, CGRectGetMaxY(topView.frame), CGRectGetWidth(view.bounds), CGRectGetWidth(view.bounds) / 3 * 1), rows: 1, columns: 3)
        topPuzzleView.setColor(UIColor(hue: 1.0, saturation: 1.0, brightness: 0.4, alpha: 1.0), forTileViewAtIndexPath: NSIndexPath(forItem: 0, inSection: 0))
        topPuzzleView.setColor(UIColor(hue: 0.7, saturation: 1.0, brightness: 0.4, alpha: 1.0), forTileViewAtIndexPath: NSIndexPath(forItem: 0, inSection: 1))
        topPuzzleView.setColor(UIColor(hue: 0.05, saturation: 0.5, brightness: 1.0, alpha: 1.0), forTileViewAtIndexPath: NSIndexPath(forItem: 0, inSection: 2))
        view.addSubview(topPuzzleView)
        
        var bottomPuzzleView = PuzzleGridView(frame: CGRectMake(0, CGRectGetMaxY(topPuzzleView.frame), CGRectGetWidth(view.bounds), CGRectGetWidth(view.bounds) / 3 * 1), rows: 1, columns: 3)
        bottomPuzzleView.setColor(UIColor(hue: 0.95, saturation: 0.55, brightness: 0.8, alpha: 1.0), forTileViewAtIndexPath: NSIndexPath(forItem: 0, inSection: 0))
        bottomPuzzleView.setColor(UIColor(hue: 0.25, saturation: 0.35, brightness: 1.0, alpha: 1.0), forTileViewAtIndexPath: NSIndexPath(forItem: 0, inSection: 1))
        bottomPuzzleView.setColor(UIColor(hue: 0.05, saturation: 0.35, brightness: 1.0, alpha: 1.0), forTileViewAtIndexPath: NSIndexPath(forItem: 0, inSection: 2))
        view.addSubview(bottomPuzzleView)
        
        var bottomView:UIView = UIView(frame: CGRectMake(0, CGRectGetMaxY(bottomPuzzleView.frame), CGRectGetWidth(view.frame), CGRectGetMaxY(view.bounds) - CGRectGetMaxY(bottomPuzzleView.frame)))
        bottomView.backgroundColor = UIColor(hue: 0.05, saturation: 0.45, brightness: 0.9, alpha: 1)
        view.addSubview(bottomView)
        
        self.view = view
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    func viewAtPoint(point:CGPoint) -> TileView?
    {
        
        for subview in gridView!.subviews {
            if (CGRectContainsPoint(subview.frame, point)) {
                return subview as? TileView
            }
        }
        
        return nil
    }
    
    func rotationVectorFromPoint(fromPoint:CGPoint, toPoint:CGPoint) -> CGVector
    {
        var distance:CGVector = CGVectorMake(toPoint.x - fromPoint.x, toPoint.y - fromPoint.y)
        var rotationVector:CGVector = CGVectorMake(-distance.dy, distance.dx)
        
        var vectorMagnitude:CGFloat = sqrt((rotationVector.dx * rotationVector.dx) + (rotationVector.dy * rotationVector.dy))
        rotationVector.dx /= vectorMagnitude
        rotationVector.dy /= vectorMagnitude
        
        return rotationVector;
    }
}
