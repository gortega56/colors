//
//  GridView.swift
//  Colors
//
//  Created by Gabriel Ortega on 11/3/14.
//  Copyright (c) 2014 Clique City. All rights reserved.
//

import UIKit

class GridView: UIView, Equatable
{
    let saturation:CGFloat = 1.0
    let brightness:CGFloat = 0.8
    var rows:Int = 0
    var columns:Int = 0
    
    init(frame: CGRect, rows:Int, columns:Int)
    {
        super.init(frame: frame)
        self.rows = rows
        self.columns = columns
        
        var size:CGSize = CGSizeMake(CGRectGetWidth(frame) / CGFloat(rows), CGRectGetWidth(frame) / CGFloat(columns))
        for columnIndex in 0..<columns {
            for rowIndex in 0..<rows {
                var frame:CGRect = CGRectMake(CGFloat(rowIndex) * size.width, CGFloat(columnIndex) * size.height, size.width, size.height)
                var subview:TileView = TileView(frame: frame, color: UIColor.randomHue(ColorModel.HSB, saturation: 1.0, brightness: 1.0), indexPath: NSIndexPath(forRow: rowIndex, inSection: columnIndex))
                subview.layer.zPosition = -100;
                addSubview(subview)
            }
        }
    }
    
    class func gridViewWithGridView(gridView:GridView, frame:CGRect) -> GridView
    {
        let copyGridView = GridView(frame: frame, rows: gridView.rows, columns: gridView.columns)
        
        for columnIndex in 0..<gridView.columns {
            for rowIndex in 0..<gridView.rows {
                var indexPath:NSIndexPath = NSIndexPath(forRow: rowIndex, inSection: columnIndex)
                var copyTileView:TileView = copyGridView.tileViewAtIndexPath(indexPath)!
                var tileView:TileView = gridView.tileViewAtIndexPath(indexPath)!
                copyTileView.backgroundColor = tileView.backgroundColor
            }
        }
        
        return copyGridView
    }
    
    func updateWithGridView(gridView:GridView)
    {
        if let tileViews = gridView.subviews as? [TileView] {
            for tileView:TileView in tileViews {
                var thisTileView:TileView = tileViewAtIndexPath(tileView.indexPath)!
                thisTileView.backgroundColor = tileView.backgroundColor
                thisTileView.hidden = tileView.hidden
            }
        }
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
    }
    
    func updateWithGridActionList(gridActionList:Array<GridAction>, animated:Bool)
    {
        if let tileViews = subviews as? [TileView] {
            
            for tileView in tileViews {
                tileView.hidden = true
            }
        }
        
        
        for gridAction in gridActionList {
            gridAction.performColorBlendAnimated(animated)
            gridAction.destinationTileView.hidden = false
        }
    }
    
    func tileViewAtIndexPath(indexPath:NSIndexPath) -> TileView?
    {
        if let tileViews = subviews as? [TileView] {
            
           return tileViews.filter {m in m.indexPath == indexPath}.first
        }
        
        return nil
    }
    
    func randomTileViewWithFilter(filter:(TileView)->Bool) -> TileView?
    {
        if let tileViews = subviews as? [TileView] {
            var filteredViews:Array<TileView> = tileViews.filter(filter)

            var index:Int = random() % filteredViews.count
            return filteredViews[index]
        }
        
        return nil
    }
    
    func adjacentTileViewsForTileView(tileView:TileView) -> Array<TileView>
    {
        var adjacentTileViews:Array<TileView> = []
        
        if let aTileView:TileView = tileViewAtIndexPath(NSIndexPath(forRow: tileView.indexPath.row - 1, inSection: tileView.indexPath.section)) {
            adjacentTileViews.append(aTileView)
        }
        
        if let aTileView:TileView = tileViewAtIndexPath(NSIndexPath(forRow: tileView.indexPath.row + 1, inSection: tileView.indexPath.section)) {
            adjacentTileViews.append(aTileView)
        }
        
        if let aTileView:TileView = tileViewAtIndexPath(NSIndexPath(forRow: tileView.indexPath.row, inSection: tileView.indexPath.section - 1)) {
            adjacentTileViews.append(aTileView)
        }
        
        if let aTileView:TileView = tileViewAtIndexPath(NSIndexPath(forRow: tileView.indexPath.row, inSection: tileView.indexPath.section + 1)) {
            adjacentTileViews.append(aTileView)
        }
        
        return adjacentTileViews
    }
    
   
}

func == (left:GridView, right:GridView) -> Bool
{
    if (left.subviews.count != right.subviews.count) {
        return false;
    }
    
    if let leftTileViews = left.subviews as? Array<TileView> {
        for tileView:TileView in leftTileViews {
            if (tileView != right.tileViewAtIndexPath(tileView.indexPath)) {
                return false;
            }
        }
    }
    
    return true;
}
