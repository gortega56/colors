//
//  HSBViewController.swift
//  Colors
//
//  Created by Gabriel Ortega on 9/27/14.
//  Copyright (c) 2014 Clique City. All rights reserved.
//

import UIKit



class HSBViewController: UIViewController {

    let maxHue:CGFloat = 1.0
    
    
    override func loadView() {
        
        var containerView:UIView = UIView(frame: UIScreen.mainScreen().bounds)
        var hueViewSize = CGSizeMake(CGRectGetWidth(containerView.bounds), CGRectGetHeight(containerView.bounds)/100)
        var contentOffset:CGPoint = CGPointMake(0, 0)
        var hue:CGFloat = 0.0
        while hue <= maxHue {
            var hueView = UILabel(frame:CGRectMake(contentOffset.x, contentOffset.y, hueViewSize.width, hueViewSize.height))
            hueView.backgroundColor = UIColor(hue: hue, saturation: 1.0, brightness: 1.0, alpha: 1.0)
            hueView.text = "HUE:\(hue)";
            hueView.font = UIFont.systemFontOfSize(4)
            containerView.addSubview(hueView)
            contentOffset.y += hueViewSize.height
            hue+=0.01
        }
        
        
        self.view = containerView;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    



}
