//
//  ViewController.swift
//  Colors
//
//  Created by Gabriel Ortega on 9/22/14.
//  Copyright (c) 2014 Clique City. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    let selectionView:TileView = TileView(frame: CGRectZero)
    let rotationAngle:CGFloat = 30.0 * CGFloat(M_PI / 180.0)
    let angleBound:CGFloat = 20.0
    let cModel = ColorModel.HSB
    let numberOfColors:Int = 16
    let saturation:CGFloat = 1.0
    let brightness:CGFloat = 1.0
    
    let kLabelHeight:CGFloat = 20.0
    let kPlaceHolderViewHeight:CGFloat = 80.0
    let kStripeViewHeight:CGFloat = 25.0
    let collectionView:UICollectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: UICollectionViewFlowLayout())
    let kCollectionViewCellIdentifier:String = "kCollectionViewCellIdentifier"
    let kGridViewListCount:Int = 3
    let kGridDimensions:(rows:Int, columns:Int) = (4, 4)
    
    
    var gridViewList:Array<GridView> = []
    var gestureStartPoint:CGPoint = CGPointMake(0, 0)
    var colorView:GridView = GridView(frame: CGRectZero, rows: 0, columns: 0)
    var scoreLabel:UILabel = UILabel(frame: CGRectZero)
    var timerLabel:UILabel = UILabel(frame: CGRectZero)
    var stripeView:UIView = UIView(frame: CGRectZero)
    var timer:NSTimer = NSTimer()
    var timeLimit:NSTimeInterval = 20
    var timeElapsed:NSTimeInterval = 0

    
    override func loadView()
    {
        var container:UIView = UIView(frame: UIScreen.mainScreen().applicationFrame)
        
        
        
        collectionView.frame = CGRectMake(0, 30 , CGRectGetWidth(container.bounds), CGRectGetHeight(container.bounds) * 0.125)
        (collectionView.collectionViewLayout as UICollectionViewFlowLayout).scrollDirection = UICollectionViewScrollDirection.Horizontal
        collectionView.registerClass(CollectionViewCell.self, forCellWithReuseIdentifier: kCollectionViewCellIdentifier)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.scrollEnabled = false;
        collectionView.backgroundColor = container.backgroundColor;
        container.addSubview(collectionView)
        
        scoreLabel.frame = CGRectMake(10, CGRectGetMaxY(collectionView.frame) + 10, CGRectGetWidth(container.bounds) * 0.5 - 10, kLabelHeight)
        scoreLabel.font = UIFont.inversionzWithSize(30)
      //  scoreLabel.text = "SCORE "
        container.addSubview(scoreLabel)
        
        timerLabel.frame = CGRectMake(CGRectGetMaxX(scoreLabel.frame), CGRectGetMinY(scoreLabel.frame), CGRectGetWidth(scoreLabel.frame), kLabelHeight)
        timerLabel.font = UIFont.inversionzWithSize(30)
     //   timerLabel.text = "TIMER "
        timerLabel.textAlignment = NSTextAlignment.Right;
        container.addSubview(timerLabel)
        
//        stripeView.frame = CGRectMake(0, CGRectGetMaxY(collectionView.frame) + 20, CGRectGetWidth(container.bounds), kStripeViewHeight)
//        stripeView.backgroundColor = UIColor.whiteColor()
//        container.addSubview(stripeView)
        
        colorView = GridView(frame: CGRectInset(CGRectMake(0, CGRectGetMaxY(scoreLabel.frame), CGRectGetWidth(container.bounds), CGRectGetWidth(container.bounds)), 10, 10), rows: 3, columns: 3)
        container.addSubview(colorView)
        
        var panGestureRecognizer:UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: "panGestureRecognized:")
        colorView.addGestureRecognizer(panGestureRecognizer)

        var quitButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        quitButton.addTarget(self, action: "tapQuitButton:", forControlEvents: UIControlEvents.TouchUpInside)
        quitButton.setTitle("quit", forState: UIControlState.Normal)
        quitButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        quitButton.sizeToFit()
        quitButton.frame.origin = CGPointMake(CGRectGetMidX(container.frame) - CGRectGetWidth(quitButton.bounds) * 0.5, CGRectGetMaxY(container.frame) - (CGRectGetHeight(quitButton.bounds) + 20))
        container.addSubview(quitButton)
        self.view = container
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        resetTimer()
        reloadCollectionViewGrids()
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func resetTimer() -> Void
    {
        self.timeElapsed = 0
    //    self.timer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "updateTimerLabel:", userInfo: nil, repeats: true)
    }
    
    func updateTimerLabel(timer:NSTimer) -> Void
    {
        timeElapsed += timer.timeInterval
        timerLabel.text = "TIME \(timeLimit - timeElapsed)"
        
        if (timeElapsed >= timeLimit) {
            timer.invalidate()
            
            var alertController:UIAlertController = UIAlertController(title: "LOSER", message: "", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "RESET", style: UIAlertActionStyle.Default, handler: { (alertAction:UIAlertAction?) -> Void in
                
                self.dismissViewControllerAnimated(true, completion:nil)
                self.resetTimer()
                self.reloadCollectionViewGrids()
            }))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func reloadCollectionViewGrids() -> Void
    {
        gridViewList.removeAll(keepCapacity: true)
        for index in 0..<kGridViewListCount {
            var gridViewCopy:GridView = GridView.gridViewWithGridView(colorView, frame: CGRectZero)
            var gridActions:Array<GridAction> = createActionListForGridView(gridViewCopy, withCount: 3)
            gridViewCopy.updateWithGridActionList(gridActions, animated: false)
            gridViewList.append(gridViewCopy)
        }
        
        collectionView.reloadData()
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func panGestureRecognized(panGestureRecognizer:UIPanGestureRecognizer) {
       
        var gestureLocation:CGPoint = panGestureRecognizer.locationInView(panGestureRecognizer.view);
        
        switch panGestureRecognizer.state {
            case UIGestureRecognizerState.Began:
                if let selectedView:TileView = viewAtPoint(gestureLocation) {
                    selectionView.frame = selectedView.frame
                    selectionView.backgroundColor = selectedView.backgroundColor
                    colorView.addSubview(selectionView)
                    gestureStartPoint = selectionView.center
                    
                    var rotationVector:CGVector = rotationVectorFromPoint(gestureLocation, toPoint: gestureStartPoint)
                    selectionView.rotateAboutAxis(rotationVector)
                }
                break
            case UIGestureRecognizerState.Changed:
                var rotationVector:CGVector = rotationVectorFromPoint(gestureLocation, toPoint: gestureStartPoint)
                selectionView.rotateAboutAxis(rotationVector)
                
                if let sublayers = stripeView.layer.sublayers {
                    
                    for index in 0..<stripeView.layer.sublayers.count {
                        var sublayer = stripeView.layer.sublayers[index] as CALayer
                        sublayer.removeFromSuperlayer()
                    }
                        
                }
                
                if let view = viewAtPoint(gestureLocation) {
                    var sourceColor:CGColorRef = colorAtPoint(gestureStartPoint).CGColor
                    var destinationColor:CGColorRef = colorAtPoint(gestureLocation).CGColor
                    var gradientLayer:CAGradientLayer = CAGradientLayer()
                
                    gradientLayer.frame = stripeView.bounds;
                    gradientLayer.colors = [sourceColor, destinationColor]
                    gradientLayer.startPoint = CGPointMake(0.0, 0.5)
                    gradientLayer.endPoint = CGPointMake(1.0, 0.5)
                    stripeView.layer.insertSublayer(gradientLayer, atIndex: 0)
                }
                break
            case UIGestureRecognizerState.Ended:
                if let destinationView = viewAtPoint(gestureLocation) {
                    GridAction.performColorBlend(self.selectionView, destinationTileView: destinationView, withAnimations: { () -> () in
                        self.selectionView.frame = destinationView.frame
                        self.selectionView.transform = destinationView.transform
                        self.selectionView.backgroundColor = destinationView.backgroundColor
                        }, completion: { (finished) -> () in
                            self.selectionView.removeFromSuperview()
                            
                            self.checkWinCondition()
                    })
                }
                
                break
            default:
                break
        }
    }
    
    func tapQuitButton(sender:UIButton) -> Void
    {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    func checkWinCondition()
    {
        for index in 0..<gridViewList.count {
            if (colorView == gridViewList[index]) {
                
                var alertController:UIAlertController = UIAlertController(title: "WON!", message: "\(index)", preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (alertAction:UIAlertAction!) -> Void in
                    self.dismissViewControllerAnimated(true, completion: nil)
                    var timeBonus:Double = Double(index + 1) * -5.0
                    self.timeElapsed.advancedBy(timeBonus)
                    self.reloadCollectionViewGrids()

                }))
                
                self.presentViewController(alertController, animated: true, completion:nil)
                return
            }
        }
    }
    
    func checkWinConditionWithGridView(gridView:GridView) -> Bool
    {
        for columnIndex in 0..<gridView.columns {
            for rowIndex in 0..<gridView.rows {
                var indexPath:NSIndexPath = NSIndexPath(forRow: rowIndex, inSection: columnIndex)
                var playerTileView:TileView = colorView.tileViewAtIndexPath(indexPath)!
                var tileView:TileView = gridView.tileViewAtIndexPath(indexPath)!
                if (playerTileView != tileView) {
                    return false
                }
            }
        }
        
        return true
    }
    
    func colorAtPoint(point:CGPoint) -> UIColor!
    {
        var color:UIColor? = UIColor.clearColor()
        
        for subview in colorView.subviews as [UIView] {
            if (CGRectContainsPoint(subview.frame, point)) {
                var hue:CGFloat = 0
                subview.backgroundColor!.getHue(&hue, saturation: nil, brightness: nil, alpha: nil)
                return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
            }
        }
        
        if let returnColor = color {
            return returnColor
        }
        else {
            return nil
        }
    }

    func viewAtPoint(point:CGPoint) -> TileView?
    {
        
        for subview in colorView.subviews {
            if (CGRectContainsPoint(subview.frame, point)) {
                return subview as? TileView
            }
        }
        
        return nil
    }
    
    func rotationVectorFromPoint(fromPoint:CGPoint, toPoint:CGPoint) -> CGVector
    {
        var distance:CGVector = CGVectorMake(toPoint.x - fromPoint.x, toPoint.y - fromPoint.y)
        
        var rotationVector:CGVector = CGVectorMake(-distance.dy, distance.dx)
        
        var vectorMagnitude:CGFloat = sqrt((rotationVector.dx * rotationVector.dx) + (rotationVector.dy * rotationVector.dy))
        rotationVector.dx /= vectorMagnitude
        rotationVector.dy /= vectorMagnitude

        return rotationVector;
    }
    
    func createActionListForGridView(gridView:GridView, withCount count:Int) -> [GridAction]
    {
        var gridActions:Array<GridAction> = []
        
        var indices:Array<NSIndexPath> = []
        for tileView:TileView in gridView.subviews as [TileView] {
            indices.append(tileView.indexPath)
        }
        
        for action in 0..<count {
            var randomIndex = random() % indices.count
            var sourceTileView:TileView = gridView.tileViewAtIndexPath(indices[randomIndex])!
            
            var adjacentTileViews = gridView.adjacentTileViewsForTileView(sourceTileView)
            var randomDestinationIndex = random() % adjacentTileViews.count
            var destinationTileView:TileView = adjacentTileViews[randomDestinationIndex]
            indices.removeAtIndex(randomDestinationIndex)
            
            gridActions.append(GridAction(sourceTileView: sourceTileView, destinationTileView: destinationTileView))
        }

        return gridActions
    }

    func copyGridView(gridView:GridView, withActionCount actionCount:Int) -> GridView
    {
        var gridViewCopy:GridView = GridView.gridViewWithGridView(gridView, frame: CGRectZero)
        var gridActions:Array<GridAction> = createActionListForGridView(gridViewCopy, withCount: actionCount)
        gridViewCopy.updateWithGridActionList(gridActions, animated: false)
        
        return gridViewCopy
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return gridViewList.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        
        return CGSizeMake(CGRectGetWidth(collectionView.bounds) * 0.333, CGRectGetHeight(collectionView.bounds))
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        
        var collectionViewCell:CollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier(kCollectionViewCellIdentifier, forIndexPath: indexPath) as CollectionViewCell
        collectionViewCell.updateGridViewWithGridView(gridViewList[indexPath.row])
        
        return collectionViewCell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets
    {
        return UIEdgeInsetsZero
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat
    {
        return 0
    }
}

class CollectionViewCell: UICollectionViewCell
{
    var gridView:GridView?

    override init(frame: CGRect)
    {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateGridViewWithGridView(gridView:GridView)
    {
        if (self.gridView == nil) {
            self.gridView = GridView(frame: CGRectInset(self.contentView.bounds, (CGRectGetWidth(self.contentView.bounds) - CGRectGetHeight(self.contentView.bounds)) * 0.5, 0), rows: gridView.rows, columns: gridView.columns);
            self.gridView!.layer.cornerRadius = 2.5
            self.gridView!.layer.masksToBounds = true
            self.gridView!.layer.borderWidth = 0.5
            self.gridView!.layer.borderColor = UIColor.lightGrayColor().CGColor
            self.contentView.addSubview(self.gridView!)
        }
        
        self.gridView?.updateWithGridView(gridView)
    }
    
    func updateGridViewWithActions(gridActions:Array<GridAction>)
    {
        
    }

}
