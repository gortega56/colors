//
//  MainMenuViewController.swift
//  Colors
//
//  Created by Gabriel Ortega on 12/16/14.
//  Copyright (c) 2014 Clique City. All rights reserved.
//

import UIKit

class MainMenuViewController: UIViewController {

    override func loadView()
    {
        var view:UIView = UIView(frame: UIScreen.mainScreen().bounds)
        
        var font:UIFont = UIFont.inversionzWithSize(60)
        
        var title:NSMutableAttributedString = NSMutableAttributedString(string: "COLORS", attributes:[NSFontAttributeName : font, NSForegroundColorAttributeName : UIColor.whiteColor()])
        title.addAttribute(NSBackgroundColorAttributeName, value: UIColor.redColor(), range: NSRange(location: 0, length: 1))
        title.addAttribute(NSBackgroundColorAttributeName, value: UIColor.yellowColor(), range: NSRange(location: 1, length: 1))
        title.addAttribute(NSBackgroundColorAttributeName, value: UIColor.greenColor(), range: NSRange(location: 2, length: 1))
        title.addAttribute(NSBackgroundColorAttributeName, value: UIColor.cyanColor(), range: NSRange(location: 3, length: 1))
        title.addAttribute(NSBackgroundColorAttributeName, value: UIColor.blueColor(), range: NSRange(location: 4, length: 1))
        title.addAttribute(NSBackgroundColorAttributeName, value: UIColor.magentaColor(), range: NSRange(location: 5, length: 1))
        
        var titleLabel:UILabel = UILabel(frame: CGRectZero);
        titleLabel.attributedText = title
        titleLabel.sizeToFit()
        titleLabel.frame.origin = CGPointMake(CGRectGetMidX(view.bounds) - CGRectGetWidth(titleLabel.bounds) * 0.5, CGRectGetMidY(view.bounds) - CGRectGetHeight(titleLabel.bounds) * 0.5)
        view.addSubview(titleLabel)

        var tutorialButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        tutorialButton.addTarget(self, action: "tapTutorialButton:", forControlEvents: UIControlEvents.TouchUpInside)
        tutorialButton.setTitle("tutorial", forState: UIControlState.Normal)
        tutorialButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        tutorialButton.sizeToFit()
        tutorialButton.frame = CGRectMake(CGRectGetMinX(titleLabel.frame), CGRectGetMaxY(titleLabel.frame) + 20, CGRectGetWidth(titleLabel.bounds), CGRectGetHeight(titleLabel.bounds))

        view.addSubview(tutorialButton)
        
        var startButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        startButton.addTarget(self, action: "tapStartButton:", forControlEvents: UIControlEvents.TouchUpInside)
        startButton.setTitle("start", forState: UIControlState.Normal)
        startButton.setTitleColor(UIColor.lightGrayColor(), forState: UIControlState.Normal)
        startButton.frame = CGRectMake(CGRectGetMinX(tutorialButton.frame), CGRectGetMaxY(tutorialButton.frame) + 10, CGRectGetWidth(tutorialButton.bounds), CGRectGetHeight(tutorialButton.bounds))
        view.addSubview(startButton)
        
        self.view = view
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)

    }
    
    override func viewDidAppear(animated: Bool)
    {
        super.viewDidAppear(animated)
        
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.view.alpha = 1.0
        })
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    func tapTutorialButton(sender:UIButton) -> Void
    {
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.view.alpha = 0.0
            }) { (finished) -> Void in
                var gameViewController:PuzzleViewController = PuzzleViewController(nibName: NSString?(), bundle: NSBundle?())
                self.navigationController?.pushViewController(gameViewController, animated: true)
        }
    }

    func tapStartButton(sender:UIButton) -> Void
    {
        UIView.animateWithDuration(0.25, animations: { () -> Void in
            self.view.alpha = 0.0
        }) { (finished) -> Void in
            var gameViewController:ViewController = ViewController(nibName: NSString?(), bundle: NSBundle?())
            self.navigationController?.pushViewController(gameViewController, animated: true)
        }
    }
}


