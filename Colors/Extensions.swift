//
//  Extensions.swift
//  Colors
//
//  Created by Gabriel Ortega on 1/6/15.
//  Copyright (c) 2015 Clique City. All rights reserved.
//

import UIKit

enum ColorModel {
    case RGB
    case HSB
}

extension UIFont
{
    class func inversionzWithSize(size:CGFloat) -> UIFont!
    {
        return UIFont(name: "Inversionz", size: size)
    }
}

extension UIColor
{
    class func randomHue(colorModel:ColorModel, saturation:CGFloat, brightness:CGFloat) -> UIColor {
        switch colorModel {
        case .RGB:
            var random:Int = (Int)(arc4random() % 3)
            var list:[CGFloat] = [0,0,0]
            
            list[random] = (CGFloat)(arc4random() % 128) / 128.0
            random += (Int)(arc4random() % 2) + 1
            random = random % 3
            
            list[random] = ((CGFloat)(arc4random() % 128) / 128.0) + 128
            return UIColor(red:list[0], green:list[1], blue:list[2], alpha:1)
        case .HSB:
            var hue:CGFloat = (CGFloat)(arc4random() % 100) / 100
            return UIColor(hue:hue, saturation:saturation, brightness:brightness, alpha:1.0)
        }
    }
    
    class func combineColors(color1:UIColor, color2:UIColor, colorModel:ColorModel) -> UIColor {
        switch colorModel {
        case .RGB:
            return combineRGBColors(color1, color2: color2)
        case .HSB:
            return combineHSBColors(color1, color2: color2)
        }
    }
    
    class func combineHSBColors(color1:UIColor, color2:UIColor) -> UIColor {
        var returnHue:CGFloat = 0
        var hue1:CGFloat = 0
        var hue2:CGFloat = 0
        var saturation1:CGFloat = 0
        var saturation2:CGFloat = 0
        var brightness1:CGFloat = 0
        var brightness2:CGFloat = 0
        
        color1.getHue(&hue1, saturation: &saturation1, brightness: &brightness1, alpha: nil)
        color2.getHue(&hue2, saturation: &saturation2, brightness: &brightness2, alpha: nil)
        
        func LERP(v0:CGFloat, v1:CGFloat, t:CGFloat) -> CGFloat {
            return ((1 - t) * v0) + (t * v1);
        }
        
        var hue = LERP(hue1, hue2, 0.5)
        var deltaHue = fabs(hue2 - hue1)
        if (deltaHue > 0.5) {
            if (hue <= 0.5) {
                hue += 0.5
            }
            else {
                hue -= 0.5
            }
        }
        
        var saturation = LERP(saturation1, saturation2, 0.5)
        var brightness = LERP(brightness1, brightness2, 0.5)
        
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1.0)
    }
    
    class func combineRGBColors(color1:UIColor, color2:UIColor) -> UIColor {
        var red1:CGFloat = 0
        var green1:CGFloat = 0
        var blue1:CGFloat = 0
        var alpha1:CGFloat = 0
        
        var red2:CGFloat = 0
        var green2:CGFloat = 0
        var blue2:CGFloat = 0
        var alpha2:CGFloat = 0
        
        var bool1 = color1.getRed(&red1, green: &green1, blue: &blue1, alpha: &alpha1)
        var bool2 = color2.getRed(&red2, green: &green2, blue: &blue2, alpha: &alpha2)
        
        var components = CGColorGetComponents(color1.CGColor)
        alpha1 *= 0.5
        
        var alpha:CGFloat = (1 - (1 - alpha1) * (1 - alpha2))
        if (alpha < 1.0e-6) {
            return UIColor.clearColor()
        }
        var red:CGFloat = red1 * alpha1 / alpha + red2 * alpha2 * (1 - alpha1) / alpha
        var green:CGFloat = green1 * alpha1 / alpha + green2 * alpha2 * (1 - alpha1) / alpha
        var blue:CGFloat = blue1 * alpha1 / alpha + blue2 * alpha2 * (1 - alpha1) / alpha
        
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
}