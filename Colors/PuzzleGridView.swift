//
//  PuzzleGridView.swift
//  Colors
//
//  Created by Gabriel Ortega on 1/6/15.
//  Copyright (c) 2015 Clique City. All rights reserved.
//

import UIKit

class PuzzleGridView: UIView
{
    var columns:Int = 0
    var rows:Int = 0
    var selectedIndexPath:NSIndexPath?
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(frame: CGRect, rows:Int, columns:Int)
    {
        super.init(frame: frame)
        self.columns = columns
        self.rows = rows
        
        var size:CGSize = CGSizeMake(CGRectGetWidth(frame) / CGFloat(columns), CGRectGetWidth(frame) / CGFloat(columns))
        for columnIndex in 0..<columns {
            for rowIndex in 0..<rows {
                var frame:CGRect = CGRectMake(CGFloat(columnIndex) * size.width, (CGRectGetMidY(self.bounds) -  size.height * CGFloat(rows) * 0.5) + CGFloat(rowIndex) * size.height, size.width, size.height)
                var subview:TileView = TileView(frame: frame, color: UIColor.randomHue(ColorModel.HSB, saturation: 0.8, brightness: 0.8), indexPath: NSIndexPath(forRow: rowIndex, inSection: columnIndex))
                subview.layer.zPosition = -100;
                addSubview(subview)
            }
        }
        
        var tapGestureRecognizer:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "tapGestureRecognized:")
        self.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func tapGestureRecognized(tapGestureRecognizer:UITapGestureRecognizer) -> Void
    {
        var tapPoint:CGPoint = tapGestureRecognizer.locationInView(self)
        if let tappedTileView:TileView = self.viewAtPoint(tapPoint) {
            tappedTileView.layer.borderColor = UIColor.whiteColor().CGColor
            tappedTileView.layer.borderWidth = 5.0
            
            if (selectedIndexPath == nil) {
                selectedIndexPath = tappedTileView.indexPath
            }
            else {
                UIView.animateWithDuration(0.25, delay: 0.5, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
                    if let selectedTileView:TileView = self.tileViewAtIndexPath(self.selectedIndexPath!) {
                        var color:UIColor = UIColor.combineHSBColors(selectedTileView.backgroundColor!, color2: tappedTileView.backgroundColor!)
                        selectedTileView.backgroundColor = color
                        tappedTileView.backgroundColor = color
                        selectedTileView.layer.borderWidth = 0.0
                        tappedTileView.layer.borderWidth = 0.0
                    }
                }, completion: { (finished) -> Void in
                    self.selectedIndexPath = nil
                })
            }
        }
    }
    
    func viewAtPoint(point:CGPoint) -> TileView?
    {
        
        for subview in self.subviews {
            if (CGRectContainsPoint(subview.frame, point)) {
                return subview as? TileView
            }
        }
        
        return nil
    }
    
    func tileViewAtIndexPath(indexPath:NSIndexPath) -> TileView?
    {
        if let tileViews = subviews as? [TileView] {
            return tileViews.filter {m in m.indexPath == indexPath}.first
        }
        
        return nil
    }
    
    func setColor(color:UIColor, forTileViewAtIndexPath indexPath:NSIndexPath) -> Void
    {
        if let tileView:TileView = tileViewAtIndexPath(indexPath) {
            tileView.backgroundColor = color
        }
    }
}
