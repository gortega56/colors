//
//  GridAction.swift
//  Colors
//
//  Created by Gabriel Ortega on 11/9/14.
//  Copyright (c) 2014 Clique City. All rights reserved.
//

import Foundation
import UIKit



class GridAction
{
    var sourceTileView:TileView
    var destinationTileView:TileView
    
    init (sourceTileView:TileView, destinationTileView:TileView)
    {
        self.sourceTileView = sourceTileView
        self.destinationTileView = destinationTileView
    }
    
    class func performColorBlend(sourceTileView:TileView, destinationTileView:TileView, withAnimations animations:(()->())?, completion:(finished:Bool)->())
    {
        let gridAction:GridAction = GridAction(sourceTileView: sourceTileView, destinationTileView: destinationTileView)
        gridAction.performColorBlendWithCustomAnimation(animations, completion: completion)
    }
    
    class func performColorBlend(sourceTileView:TileView, destinationTileView:TileView, animated:Bool) {
        let gridAction:GridAction = GridAction(sourceTileView: sourceTileView, destinationTileView: destinationTileView)
        gridAction.performColorBlendAnimated(animated)
    }

    func performColorBlendWithCustomAnimation(customAnimation:(()->())?, completion:(finished:Bool)->())
    {
        func animations()-> Void {
            performColorBlendAnimated(false)
            if customAnimation != nil {
                customAnimation!()
            }
        }
        
        UIView.animateWithDuration(0.25, animations: animations, completion: completion)
    }
    
    
    func performColorBlendAnimated(animated:Bool)
    {
        func animations()-> Void {
            var color:UIColor = UIColor.combineHSBColors(sourceTileView.backgroundColor!, color2: destinationTileView.backgroundColor!)
            destinationTileView.backgroundColor = color
        }
        
        animated ? UIView.animateWithDuration(0.25, animations: animations, completion: nil) : animations()
    }
    
}